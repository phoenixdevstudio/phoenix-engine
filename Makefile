#compiler
CC = g++
#compiler flags
CFLAGS = -g
#build target executable
TARGET = versuch
#source files
SOURCE = *.cpp src/*.cpp

all: $(TARGET)

$(TARGET):(TARGET).cpp
	$(CC) $(CFLAGS) $(SOURCE) -o $(TARGET )