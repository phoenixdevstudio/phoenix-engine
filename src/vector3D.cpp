#include "vector3D.h"

vector3D::vector3D() {
    x = 0.f;
    y = 0.f;
    z = 0.f;
}

vector3D::vector3D(float _x, float _y, float _z){
	this->x = _x;
	this->y = _y;
    this->z = _z;
}

void vector3D::add_vector3D(vector3D* _){
    x += _->x;
    y += _->y;
    z += _->z;
}

void vector3D::subtract_vector3D(vector3D* _){
    x -= _->x;
    y -= _->y;
    z -= _->z;
}

void vector3D::mutliply_vector3D(vector3D* _){
    x *= _->x;
    y *= _->y;
    z *= _->z;
}

void vector3D::divide_vector3D(vector3D* _){
    x /= _->x;
    y /= _->y;
    z /= _->z;
}