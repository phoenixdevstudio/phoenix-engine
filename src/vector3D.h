#ifndef VECTOR3D_H
#define VECTOR3D_H

class vector3D {
public:
    float x, y, z;

    vector3D();
    vector3D(float _x, float _y, float _z);

    void add_vector3D(vector3D* _);
    void subtract_vector3D(vector3D* _);
    void mutliply_vector3D(vector3D* _);
    void divide_vector3D(vector3D* _);
};

#endif

