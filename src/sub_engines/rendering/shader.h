#ifndef SHADER_H
#define SHADER_H

#include <string>
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <GL/glu.h>
#include <SDL2/SDL_opengl.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "../transform.h"

class shader{
private:
    std::string load_shader(const std::string& fileName);

    enum{
        TRANSFORM_U,

        NUM_UNIFORMS
    };

    GLuint m_uniforms[NUM_UNIFORMS];

public:
    GLuint program;
    shader();
    shader(const std::string& name);
    ~shader();

    void shader_init(const std::string& name);

    //void update(const transform& transform);

    void destroy();

    void use();

    void setMat4(const std::string &name, const glm::mat4 &mat) const;

    GLuint create_shader(const std::string& text, unsigned int type);
};

#endif