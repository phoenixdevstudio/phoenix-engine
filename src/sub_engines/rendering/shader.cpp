#include <iostream>
#include <fstream>
#include <string>

#include "shader.h"

shader::shader(){}

shader::shader(const std::string& name){
    shader_init(name);
}

shader::~shader(){destroy();}

std::string shader::load_shader(const std::string& fileName){
    std::ifstream file;
    file.open((fileName).c_str());

    std::string output;
    std::string line;

    if(file.is_open())
    {
        while(file.good())
        {
            getline(file, line);
			output.append(line + "\n");
        }
    }
    else
    {
		std::cerr << "Unable to load shader: " << fileName << std::endl;
    }

    return output;
}

void shader::shader_init(const std::string& name){
    program = glCreateProgram();
    GLuint vertex_shader = create_shader(load_shader(name + ".vs"), GL_VERTEX_SHADER);
    GLuint fragment_shader = create_shader(load_shader(name + ".fs"), GL_FRAGMENT_SHADER);
    glAttachShader(program,vertex_shader);
    glAttachShader(program,fragment_shader);

    glLinkProgram(program);
    glValidateProgram(program);

    glDeleteShader(vertex_shader);
    glDeleteShader(fragment_shader);

    m_uniforms[TRANSFORM_U] = glGetUniformLocation(program,"transform");
}

/*void shader::update(const transform& transform){

    glm::mat4 model = transform.GetModel();

    glUniformMatrix4fv(m_uniforms[TRANSFORM_U],1,GL_FALSE, &model[0][0]);
}*/

void shader::destroy(){
    glDeleteProgram(program);
}

void shader::use(){
    glUseProgram(program);
}

GLuint shader::create_shader(const std::string& text, unsigned int type){

    GLuint shader = glCreateShader(type);

    /*bind sourcecode to shader*/
    const GLchar* p[1];
    p[0] = text.c_str();
    glShaderSource(shader, 1, p, NULL);
    /*compile shader*/
    glCompileShader(shader);

    int result;
    glGetShaderiv(shader,GL_COMPILE_STATUS,&result);
    if (result == GL_FALSE){
        int length;
        glGetShaderiv(shader,GL_INFO_LOG_LENGTH,&length);
        GLchar* InfoLog = new GLchar[length+1];
        glGetShaderInfoLog(shader, length, &length, InfoLog);
        fprintf(stderr, "Compilation error %s\n", InfoLog);
    }

    return shader;
}

void shader::setMat4(const std::string &name, const glm::mat4 &mat) const
{
    glUniformMatrix4fv(glGetUniformLocation(program, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}
