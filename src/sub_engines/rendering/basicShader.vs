#version 130
in vec3 pos;
in vec3 color;

out vec3 Color;


uniform mat4 transform;
uniform mat4 view;
uniform mat4 model;

void main()
{
	Color = color;
	gl_Position = transform * view * model * vec4(pos,1.0);
	}