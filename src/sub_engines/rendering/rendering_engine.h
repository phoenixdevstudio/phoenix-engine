#ifndef RENDERING_ENGINE_H
#define RENDERING_ENGINE_H

#include <iostream>
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <GL/glu.h>
#include <SDL2/SDL_opengl.h>

//#include "../../objects/camera_obj.h"
#include "../object_engine.h"


class rendering_engine{
private:
    SDL_Renderer *renderer;
    object_engine* object_manager;
    //camera_obj* m_camera;
public:
    void init_renderer(SDL_Window* window, object_engine* _obj){
        SDL_Renderer* _renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
        renderer = _renderer;
        object_manager = _obj;

        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        /*render only one side*/
        //glFrontFace(GL_CW);
        //glCullFace(GL_BACK);
        //glEnable(GL_CULL_FACE);
        
        glEnable(GL_DEPTH_TEST);

        glEnable(GL_FRAMEBUFFER_SRGB);
    }

    void clear_screen(){
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    void render(){

    }

};

#endif