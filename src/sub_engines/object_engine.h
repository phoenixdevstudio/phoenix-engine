#ifndef OBJECT_ENGINE_H
#define OBJECT_ENGINE_H

#include <iostream>
#include <vector>
#include <memory>

#include "../objects/objects.h"

class object_engine {
public:
    std::vector<std::unique_ptr<objects>> _objects;
    void add_object(objects* _obj){
        std::unique_ptr<objects> uPtr{_obj};
        _objects.emplace_back(std::move(uPtr));
    }
};

#endif