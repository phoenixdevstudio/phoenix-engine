#include "game.h"
#include "sub_engines/rendering/rendering_engine.h"
#include "sub_engines/object_engine.h"
#include "objects/objects.h"


void game::add_object(objects* _obj){
    objects_manager->add_object(_obj);
}

void game::render(rendering_engine* _renderer){
    _renderer->render();
}