#ifndef CAMERA_OBJ_H
#define CAMERA_OBJ_H

#include "../vector3D.h"
#include "../vector2D.h"
#include "../core_engine.h"

class camera_obj{
public:
    bool active = true;
    vector3D* location;
    vector2D* frame;
    void create_camera(vector3D* _location){
        location = _location;
        frame = new vector2D(800.f, 480.f);
    }
};

#endif