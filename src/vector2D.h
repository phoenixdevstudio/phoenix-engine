#ifndef VECTOR2D_H
#define VECTOR2D_H

class vector2D {
public:
    float x, y;

    vector2D();
    vector2D(float _x, float _y);

    void add_vector2D(vector2D* _);
    void subtract_vector2D(vector2D* _);
    void mutliply_vector2D(vector2D* _);
    void divide_vector2D(vector2D* _);
};

#endif

