#ifndef CORE_ENGINE_H
#define CORE_ENGINE_H

#include <iostream>
#include <SDL2/SDL.h>


#include "vector2D.h"

#include "sub_engines/rendering/shader.h"
#include "sub_engines/object_engine.h"
#include "sub_engines/rendering/rendering_engine.h"
#include "game.h"

class core_engine{
private:
    vector2D* window_size;
    const char* title = "";
    bool isRunning = false;

    rendering_engine m_renderingEngine;
    SDL_Window* _w;
    game* m_game;
    shader s;

    void main_loop();

    void b_event();

    void set_attributes();

    SDL_Window* create_window(vector2D* _window_size, const char* _title);

public:

    void CoreEngine(vector2D* _window_size, const char* _title, int framerate, game* _game);
    void destroy();
};

#endif