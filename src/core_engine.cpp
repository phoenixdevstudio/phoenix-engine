#define GLM_ENABLE_EXPERIMENTAL

#include <iostream>
#include <string>

#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <GL/glu.h>
#include <SDL2/SDL_opengl.h>

#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "vector2D.h"
#include "core_engine.h"

#include "sub_engines/rendering/rendering_engine.h"

#include "sub_engines/object_engine.h"
#include "sub_engines/input_engine.h"

#include "sub_engines/rendering/shader.h"
#include "sub_engines/transform.h"

void core_engine::CoreEngine(vector2D* _window_size, const char* _title, int framerate, game* _game){
    window_size = _window_size;
    title = _title;

    set_attributes();

    _w = create_window(_window_size, _title);
    SDL_GL_CreateContext(_w);

    if (GLenum err = glewInit() != GLEW_OK) fprintf(stderr, "ERROR: %s\n", glewGetErrorString(err));

    m_game = _game;
    m_renderingEngine.init_renderer(_w, m_game->objects_manager);
    m_game->init();

    s.shader_init("src/sub_engines/rendering/basicShader");

    isRunning = true;

    main_loop();
}

void core_engine::destroy(){
    s.destroy();
    SDL_Quit();
}

void core_engine::set_attributes(){
    SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 1 );
    SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 3 ); 
    SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );

    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);

    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
    SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 16 );
    SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
    glEnable(GL_DEPTH_TEST);  
}

void core_engine::main_loop(){
    //transform t;

    std::vector< unsigned int > vertexIndices, uvIndices, normalIndices;
    std::vector< glm::vec3 > temp_vertices;
    std::vector< glm::vec2 > temp_uvs;
    std::vector< glm::vec3 > temp_normals;

    GLfloat vertices[] = {
        -0.5f, -0.5f, -0.5f,  1.0f, 0.0f,0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f,0.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 0.0f,0.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 0.0f,0.0f,
        -0.5f,  0.5f, -0.5f,  1.0f, 0.0f,0.0f,
        -0.5f, -0.5f, -0.5f,  1.0f, 0.0f,0.0f,

        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,1.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f,1.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 1.0f,1.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 1.0f,1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 1.0f,1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,1.0f,

        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,1.0f,
        -0.5f,  0.5f, -0.5f,  1.0f, 1.0f,1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,1.0f,
        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,1.0f,

         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,1.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f,1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f,1.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 0.0f,1.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,1.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,1.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 1.0f,1.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f,1.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f,1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,1.0f,

        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,1.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,1.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,1.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 0.0f,1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,1.0f

    };
    unsigned int VBO;
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);  
    
    glBufferData(GL_ARRAY_BUFFER,sizeof(vertices), vertices, GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);

    //glBufferSubData(GL_ARRAY_BUFFER, 0, 6, vertices);
    glVertexAttribPointer(0 , 3,GL_FLOAT,GL_FALSE,sizeof(float)*6,0);
    s.use();

GLint colAttrib = glGetAttribLocation(s.program, "color");
glEnableVertexAttribArray(colAttrib);
glVertexAttribPointer(colAttrib, 3, GL_FLOAT, GL_FALSE,
                       6*sizeof(float), (void*)(3*sizeof(float)));
    float x = 0;

    glm::mat4 proj = glm::perspective(glm::radians(45.0f), (float)window_size->x/(float)window_size->y, 0.1f, 100.0f);
    s.setMat4("transform", proj);
    glm::mat4 model = glm::mat4(1.0f);
    while(isRunning){
        b_event();
        m_renderingEngine.clear_screen();
        //s.update(t);
        model = glm::translate(model,glm::vec3( 0.0f,  0.0f,  0.0f));
        model = glm::rotate(model, glm::radians(0.001f), glm::vec3(0.5f, 1.0f, 0.0f));
        glm::mat4 view = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -5.0f));
        x -= 0.001f;
        s.setMat4("view", view);
        s.setMat4("model", model);
        glDrawArrays(GL_TRIANGLES, 0, 36);
        //m_game->render(&m_renderingEngine);

        SDL_GL_SwapWindow(_w);
    }
}

void core_engine::b_event(){
    SDL_Event event;
    SDL_PollEvent(&event);
    switch(event.type){
        case SDL_QUIT:
            isRunning = false;
            break;
        default:
            break;
    }
}

SDL_Window* core_engine::create_window(vector2D* _window_size, const char* _title){
    glViewport(0,0,(int)_window_size->x,(int)_window_size->y);
    return SDL_CreateWindow(_title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, (int)_window_size->x, (int)_window_size->y, SDL_WINDOW_OPENGL);
}