#include "vector2D.h"

vector2D::vector2D() {
    x = 0.0f;
    y = 0.0f;
}

vector2D::vector2D(float _x, float _y){
	this->x = _x;
	this->y = _y;
}

void vector2D::add_vector2D(vector2D* _){
    x += _->x;
    y += _->y;
}

void vector2D::subtract_vector2D(vector2D* _){
    x -= _->x;
    y -= _->y;
}

void vector2D::mutliply_vector2D(vector2D* _){
    x *= _->x;
    y *= _->y;
}

void vector2D::divide_vector2D(vector2D* _){
    x /= _->x;
    y /= _->y;
}