#ifndef GAME_H
#define GAME_H

#include "sub_engines/rendering/rendering_engine.h"

#include "sub_engines/object_engine.h"
#include "objects/objects.h"

class game{
public:
    object_engine* objects_manager;

    virtual void init(){}

    void add_object(objects* _obj);

    void render(rendering_engine* _renderer);

};

#endif